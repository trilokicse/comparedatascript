from connection import  connect_oracle
import cx_Oracle
import requests as rq
import json
import os
import sys

#from BLOB import logger_table_config, logger
import logging

#tlogger = logger_table_config()
#logger(logging)


class SDK:

    def __init__(self):
        self.db_flag = 1
        self.query_flag = 2
        self.insert_success_flag = True
        self.update_success_flag = True
        self.username = 'cod_optdev'  # os.environ.get('ORACLE_DB_USERNAME')
        self.pwd = 'codoptdev1'  # os.environ.get('ORACLE_DB_PASSWORD')
        self.host = '10.160.74.10'  # os.environ.get('ORACLE_DB_URL')
        self.port = '1567'  # os.environ.get('ORACLE_DB_PORT')
        self.sid = 'DCUST'  # os.env

        # self.db_flag = 1
        # self.query_flag = 2
        # self.insert_success_flag = True
        # self.update_success_flag = True
        # self.username = 'uormae'  # os.environ.get('ORACLE_DB_USERNAME')
        # self.pwd = 'test1'  # os.environ.get('ORACLE_DB_PASSWORD')
        # self.host = '134.209.152.8'  # os.environ.get('ORACLE_DB_URL')
        # self.port = '1523'  # os.environ.get('ORACLE_DB_PORT')
        # self.sid = 'orcl'  # os.env

        # self.db_flag = 1
        # self.query_flag = 2
        # self.insert_success_flag = True
        # self.update_success_flag = True
        # self.username = os.environ.get('ORACLE_DB_USERNAME')
        # self.pwd = os.environ.get('ORACLE_DB_PASSWORD')
        # self.host = os.environ.get('ORACLE_DB_URL')
        # self.port = os.environ.get('ORACLE_DB_PORT')
        # self.sid = os.environ.get('ORACLE_DB_SCHEMA')

        try:

            connection = connect_oracle(self.username, self.pwd, self.host, self.port, self.sid)
            cursor = connection.cursor()

            # tlogger.info ("DB connection successful in SDK")

        except Exception as e:

            #logger.error("DB connection failed")
            quit()
            # return {self.db_flag: str(sys.exc_info()[1])}

        function = """
                 CREATE OR REPLACE FUNCTION fetch_job
                 RETURN NUMBER IS
                     jid NUMBER;
                 BEGIN
                     select id into jid from job where status = 'optimise' order by createdat fetch first 1 rows only;
                     update job set status = 'optimising' where id = jid;
                     commit;
                     return jid;
                 END fetch_job;
                 """

        #executing this function to get the updated jobs

        # function = """
        #                CREATE OR REPLACE FUNCTION fetch_job
        #                RETURN NUMBER IS
        #                    jid NUMBER;
        #                BEGIN
        #                    select id into jid from job where status = 'optimise' order by createdat fetch first 1 rows only;
        #                    return jid;
        #                END fetch_job;
        #                """

        cursor.execute(function)
        connection.commit()

    def count(self,tablename,condition = None):
        """
        Function to get the tablename and sql condition to return the count of rows which satisifies the condition in the tablename

        :param tablename: String : Table name in DB
        :param where: String : where condition to filter the table
        :return: Count of the rows for the given condition and  tablename
        """


        #connecting to the DB
        try:

            connection = connect_oracle(self.username,self.pwd, self.host,self.port, self.sid)
            cursor = connection.cursor()

        except Exception as e:

            return{self.db_flag:str(sys.exc_info()[1])}


        if condition == None:

            query = "select count(*) from " + tablename

        else:
            #building the query
            query= "select count(*) from " + tablename + " where jobid =" + condition


        try:
            count  = 0
            for i in cursor.execute(query):
                count = i[0]

                # closing the connection and cursor

                connection.close()

            return count

        except Exception as  e:

            #closing the connection and cursor

            connection.close()

            return{self.query_flag:str(sys.exc_info()[1])}


    def distinctCount(self, columname, tablename, condition = None):
        # connecting to the DB
        try:

            connection = connect_oracle(self.username, self.pwd, self.host, self.port, self.sid)
            cursor = connection.cursor()

        except Exception as e:

            return {self.db_flag: str(sys.exc_info()[1])}

        if condition == None:

            query = "select count(distinct" + " " + columname + ") from " + tablename

        else:
            # building the query
            query = "select count(distinct" + " " + columname + ") from " + tablename + " where jobid = " + condition
        try:
            count  = 0
            for i in cursor.execute(query):
                count = i[0]

                # closing the connection and cursor

                connection.close()

            return count

        except Exception as  e:

            #closing the connection and cursor

            connection.close()

            return{self.query_flag:str(sys.exc_info()[1])}

    def distinctCount1(self, columname, tablename, condition = None):
        # connecting to the DB
        try:

            connection = connect_oracle(self.username, self.pwd, self.host, self.port, self.sid)
            cursor = connection.cursor()

        except Exception as e:

            return {self.db_flag: str(sys.exc_info()[1])}

        if condition == None:

            query = "select count(distinct" + " " + columname + ") from " + tablename

        else:
            # building the query
            # "select count(distinct" + " " + columname + ") from " + tablename + " where jobid = " + condition
            query = "select count(distinct invoicenumber) from \
            (select invoicenumber from optimise_invoice where jobid = " + condition + " \
            union \
            select invoicenumber from unassigned_invoice where jobid =" + condition +")"
        try:
            count  = 0
            for i in cursor.execute(query):
                count = i[0]

                # closing the connection and cursor

                connection.close()

            return count

        except Exception as  e:

            #closing the connection and cursor

            connection.close()

            return{self.query_flag:str(sys.exc_info()[1])}


    def fetch_one(self,columns,tablename,condition=None):
        """
        Function to return the first row of the table and where the condition satisfies

        :param columns : tuple : Columns in the table you want to view
        :param tablename: String : Tablename in the DB
        :param condition: String : Where condition to filter in the Table
        :return:  tuple  with values in order according to the columns selected from the given tablename, condition

        """
        # connecting to the DB
       # print(len(columns))

        if(len(columns)>1):
            cols = ""
            for i in columns:
                cols = cols + i + ","

            cols = cols[:len(cols) - 1]
        else:
            print("inelse")
            cols =str(columns)[1:len(columns)-1]


        try:

            connection = connect_oracle(self.username, self.pwd, self.host, self.port, self.sid)
            cursor = connection.cursor()

        except Exception as e:

            return {self.db_flag: str(sys.exc_info()[1])}

        if condition == None:


            query = "select " + columns + " from " + tablename + " fetch first 1 rows only"

        else:

            query = "select " + columns + " from " + tablename + " where id = " + condition + " fetch first 1 rows only"



        try:
            #print(query)
            val = 0
            for i in cursor.execute(query):
                val = i
            connection.close()
            return  val

        except Exception as  e:

            #closing the connection and cursor

            connection.close()

            return {self.query_flag: str(sys.exc_info()[1])}



    def fetch_many(self,columns,tablename,array_size,rows= -1,condition=None, conditionn = None ):
        """
        Function to fetch all the values from a given table with a given condition

        :param columns : Tuple : Columns in the table you want to view
        :param tablename: String : Tablename in the DB
        :param condition: String : Where condition to filter in the Table
        :param array_size : Integer: Array Size of the cursor : Batch size
        :param rows : Integer: Number of rows to be fetched
        :return: A list of tuples  with values in order according to the columns selected from the given tablename, condition

        """


        #connecting to the DB

        try:

            connection = connect_oracle(self.username, self.pwd, self.host, self.port, self.sid)
            cursor = connection.cursor()

        except Exception as e:

            return {self.db_flag: str(sys.exc_info()[1])}



        #converting tuples to string
        cols = ""
        for i in columns:
            cols = cols + i + ","

        cols=cols[:len(cols) - 1]

        #2 different queries : with and without where clause

        if condition == None:

            if rows == -1:
                query = "Select " + cols + " from " + tablename
            else:
                query = "Select " + cols + " from " + tablename + " fetch first {} rows only".format(rows)


        else:
            if rows ==-1:
                #query = "Select " + cols + " from " + tablename + " where " + condition
                query = "SELECT \
                o.truckregname, \
                case when o.truckregname  = v.truckregname then 'matching' else v.truckregname end as truckStatus, \
                case when o.truckregname  = v.truckregname then 'matching' else o.truckregname end as truckStatus, \
                case when o.drivercode  = v.driverempno then 'matching' else v.driverempno end as drivercodeStatus ,\
                case when o.drivercode  = v.driverempno then 'matching' else o.drivercode end as drivercodeStatus ,\
                case when o.teamcode  = v.teamcode then 'matching' else v.teamcode end as teamCodeStatus, \
                case when o.teamcode  = v.teamcode then 'matching' else o.teamcode end as teamCodeStatus, \
                case when o.truckareacode  = v.truckareacode then 'matching' else v.truckareacode end as truckAreaCodeStatus, \
                case when o.truckareacode  = v.truckareacode then 'matching' else o.truckareacode end as truckAreaCodeStatus, \
                case when o.truckstartloc  = v.truckstartloc then 'matching' else v.truckstartloc end as truckStartLocStatus, \
                case when o.truckstartloc  = v.truckstartloc then 'matching' else o.truckstartloc end as truckStartLocStatus, \
                case when o.truckendloc  = v.truckendloc then 'matching' else v.truckendloc end as truckendLocStatus, \
                case when o.truckendloc  = v.truckendloc then 'matching' else o.truckendloc end as truckendLocStatus, \
                case when o.carpenter1empno  = v.carpenter1empno then 'matching' else v.carpenter1empno end as carpenter1Status, \
                case when o.carpenter1empno  = v.carpenter1empno then 'matching' else o.carpenter1empno end as carpenter1Status, \
                case when o.carpenter2empno  = v.carpenter2empno then 'matching' else v.carpenter2empno end as carpenter2Status, \
                case when o.carpenter2empno  = v.carpenter2empno then 'matching' else o.carpenter2empno end as carpenter2Status, \
                case when o.carpenter3empno  = v.carpenter3empno then 'matching' else v.carpenter3empno end as carpenter3Status, \
                case when o.carpenter3empno  = v.carpenter3empno then 'matching' else o.carpenter3empno end as carpenter3Status, \
                case when o.carpenter4empno  = v.carpenter4empno then 'matching' else v.carpenter4empno end as carpenter4Status, \
                case when o.carpenter4empno  = v.carpenter4empno then 'matching' else o.carpenter4empno end as carpenter4Status, \
                case when o.carpenter5empno  = v.carpenter5empno then 'matching' else v.carpenter5empno end as carpenter5Status, \
                case when o.carpenter5empno  = v.carpenter5empno then 'matching' else o.carpenter5empno end as carpenter5Status, \
                case when o.carpenter6empno  = v.carpenter6empno then 'matching' else v.carpenter6empno end as carpenter6Status, \
                case when o.carpenter6empno  = v.carpenter6empno then 'matching' else o.carpenter6empno end as carpenter6Status \
                FROM optimise_vehicle o \
                INNER JOIN vehicle v \
                ON o.truckregname = v.truckregname \
                and o.jobid="+condition+"  and  v.jobid ="+conditionn+"\
                where o.jobid = " + condition
            else:
                query = "Select " + cols + " from " + tablename + " where " + condition + " fetch first {} rows only".format(rows)


        try:
            #print(query)
            final =[]
            #print(query)
            cursor.arraysize = array_size
            results = cursor.execute(query)



            for i in results:
                final.append(i)

            connection.close()

            return  final

        except Exception as  e:

            #closing the connection and cursor

            connection.close()

            return {self.query_flag: str(sys.exc_info()[1])}

    def fetch_many1(self,columns,tablename,array_size,rows= -1,condition=None, conditionn = None ):
        """
        Function to fetch all the values from a given table with a given condition

        :param columns : Tuple : Columns in the table you want to view
        :param tablename: String : Tablename in the DB
        :param condition: String : Where condition to filter in the Table
        :param array_size : Integer: Array Size of the cursor : Batch size
        :param rows : Integer: Number of rows to be fetched
        :return: A list of tuples  with values in order according to the columns selected from the given tablename, condition

        """


        #connecting to the DB

        try:

            connection = connect_oracle(self.username, self.pwd, self.host, self.port, self.sid)
            cursor = connection.cursor()

        except Exception as e:

            return {self.db_flag: str(sys.exc_info()[1])}



        #converting tuples to string
        cols = ""
        for i in columns:
            cols = cols + i + ","

        cols=cols[:len(cols) - 1]

        #2 different queries : with and without where clause

        if condition == None:

            if rows == -1:
                query = "Select " + cols + " from " + tablename
            else:
                query = "Select " + cols + " from " + tablename + " fetch first {} rows only".format(rows)


        else:
            if rows ==-1:
                #query = "Select " + cols + " from " + tablename + " where " + condition
                query = "SELECT \
                o.truckregname, \
                case when o.truckregname  = v.truckregname then 'matching' else v.truckregname end as truckStatus, \
                case when o.truckregname  = v.truckregname then 'matching' else o.truckregname end as truckStatus, \
                case when o.driverempno  = v.driverempno then 'matching' else v.driverempno end as drivercodeStatus ,\
                case when o.driverempno  = v.driverempno then 'matching' else o.driverempno end as drivercodeStatus ,\
                case when o.teamcode  = v.teamcode then 'matching' else v.teamcode end as teamCodeStatus, \
                case when o.teamcode  = v.teamcode then 'matching' else o.teamcode end as teamCodeStatus, \
                case when o.truckareacode  = v.truckareacode then 'matching' else v.truckareacode end as truckAreaCodeStatus, \
                case when o.truckareacode  = v.truckareacode then 'matching' else o.truckareacode end as truckAreaCodeStatus, \
                case when o.truckstartloc  = v.truckstartloc then 'matching' else v.truckstartloc end as truckStartLocStatus, \
                case when o.truckstartloc  = v.truckstartloc then 'matching' else o.truckstartloc end as truckStartLocStatus, \
                case when o.truckendloc  = v.truckendloc then 'matching' else v.truckendloc end as truckendLocStatus, \
                case when o.truckendloc  = v.truckendloc then 'matching' else o.truckendloc end as truckendLocStatus, \
                case when o.carpenter1empno  = v.carpenter1empno then 'matching' else v.carpenter1empno end as carpenter1Status, \
                case when o.carpenter1empno  = v.carpenter1empno then 'matching' else o.carpenter1empno end as carpenter1Status, \
                case when o.carpenter2empno  = v.carpenter2empno then 'matching' else v.carpenter2empno end as carpenter2Status, \
                case when o.carpenter2empno  = v.carpenter2empno then 'matching' else o.carpenter2empno end as carpenter2Status, \
                case when o.carpenter3empno  = v.carpenter3empno then 'matching' else v.carpenter3empno end as carpenter3Status, \
                case when o.carpenter3empno  = v.carpenter3empno then 'matching' else o.carpenter3empno end as carpenter3Status, \
                case when o.carpenter4empno  = v.carpenter4empno then 'matching' else v.carpenter4empno end as carpenter4Status, \
                case when o.carpenter4empno  = v.carpenter4empno then 'matching' else o.carpenter4empno end as carpenter4Status, \
                case when o.carpenter5empno  = v.carpenter5empno then 'matching' else v.carpenter5empno end as carpenter5Status, \
                case when o.carpenter5empno  = v.carpenter5empno then 'matching' else o.carpenter5empno end as carpenter5Status, \
                case when o.carpenter6empno  = v.carpenter6empno then 'matching' else v.carpenter6empno end as carpenter6Status, \
                case when o.carpenter6empno  = v.carpenter6empno then 'matching' else o.carpenter6empno end as carpenter6Status \
                FROM unassigned_truck o \
                INNER JOIN vehicle v \
                ON o.truckregname = v.truckregname \
                and o.jobid="+condition+"  and  v.jobid ="+conditionn+"\
                where o.jobid = " + condition
            else:
                query = "Select " + cols + " from " + tablename + " where " + condition + " fetch first {} rows only".format(rows)


        try:
            #print(query)
            final =[]
            #print(query)
            cursor.arraysize = array_size
            results = cursor.execute(query)



            for i in results:
                final.append(i)

            connection.close()

            return  final

        except Exception as  e:

            #closing the connection and cursor

            connection.close()

            return {self.query_flag: str(sys.exc_info()[1])}

    def fetch_manyy(self,columns,tablename,array_size,rows= -1,condition=None, conditionn = None):
        """
        Function to fetch all the values from a given table with a given condition

        :param columns : Tuple : Columns in the table you want to view
        :param tablename: String : Tablename in the DB
        :param condition: String : Where condition to filter in the Table
        :param array_size : Integer: Array Size of the cursor : Batch size
        :param rows : Integer: Number of rows to be fetched
        :return: A list of tuples  with values in order according to the columns selected from the given tablename, condition

        """


        #connecting to the DB

        try:

            connection = connect_oracle(self.username, self.pwd, self.host, self.port, self.sid)
            cursor = connection.cursor()

        except Exception as e:

            return {self.db_flag: str(sys.exc_info()[1])}



        #converting tuples to string
        cols = ""
        for i in columns:
            cols = cols + i + ","

        cols=cols[:len(cols) - 1]

        #2 different queries : with and without where clause

        if condition == None:

            if rows == -1:
                query = "Select " + cols + " from " + tablename
            else:
                query = "Select " + cols + " from " + tablename + " fetch first {} rows only".format(rows)


        else:
            if rows ==-1:
                #query = "Select " + cols + " from " + tablename + " where " + condition
                query = "SELECT \
                case when t.fixingtype  = a.ofixingtype then 'matching' else t.fixingtype end as fixingtypeStatus, \
                case when t.fixingtype  = a.ofixingtype then 'matching' else a.ofixingtype end as fixingtypeStatus, \
                case when t.itemqty  = a.oitemqty then 'matching' else t.itemqty end as itemqtyStatus, \
                case when t.itemqty  = a.oitemqty then 'matching' else a.oitemqty end as itemqtyStatus, \
                case when t.itemprice  = a.itemvalue then 'matching' else t.itemprice end as itemValueStatus, \
                case when t.itemprice  = a.itemvalue then 'matching' else a.itemvalue end as itemValueStatus, \
                case when t.itemvol  = a.itemvolume then 'matching' else t.itemvol end as itemVolumeStatus, \
                case when t.itemvol  = a.itemvolume then 'matching' else a.itemvolume end as itemVolumeStatus, \
                case when t.customerarea  = a.cuarea then 'matching' else t.customerarea end as CustomerareaStatus, \
                case when t.customerarea  = a.cuarea then 'matching' else a.cuarea end as CustomerareaStatus, \
                case when t.customerareacode  = a.cuareacode then 'matching' else t.customerareacode end as CustomerareacodeStatus, \
                case when t.customerareacode  = a.cuareacode then 'matching' else a.cuareacode end as CustomerareacodeStatus, \
                case when t.customername  = a.custname then 'matching' else t.customername end as CustomerNameStatus, \
                case when t.customername  = a.custname then 'matching' else a.custname end as CustomerNameStatus, \
                case when t.customerzone  = a.custzone then 'matching' else t.customerzone end as CustomerZoneStatus, \
                case when t.customerzone  = a.custzone then 'matching' else a.custzone end as CustomerZoneStatus, \
                case when t.invoiceno  = a.invnumber then 'matching' else t.invoiceno end as InvoiceNumberStatus, \
                case when t.invoiceno  = a.invnumber then 'matching' else a.invnumber end as InvoiceNumberStatus, \
                case when t.scheduleid  = a.schedulenumber then 'matching' else t.scheduleid end as ScheduleNumberStatus, \
                case when t.scheduleid  = a.schedulenumber then 'matching' else a.schedulenumber end as ScheduleNumberStatus \
                FROM (select o.invoicenumber, o.trucknumber,o.jobid,oi.itemqty as oitemqty, oi.itemvolume,o.customerarea as cuarea, \
                o.customerareacode as cuareacode,o.invoicenumber as invnumber,o.schedulenumber,o.slot as slt, \
                oi.itemcode ,oi.itemvalue, oi.itemdescription, oi.fixingtype as ofixingtype, o.customername as custname, o.customerzone as custzone \
                FROM optimise_invoice o\
                INNER JOIN optimise_item  oi\
                ON o.id = oi.optimiseinvoiceid \
                and o.jobid = oi.jobid \
                where o.jobid = "+ condition +") a \
                join  transaction t \
                on t.jobid = "+conditionn+" and a.jobid ="+condition+"\
                and t.invoiceno = a.invoicenumber \
                and t.itemcode = a.itemcode \
                order by a.invoicenumber desc"
            else:
                query = "Select " + cols + " from " + tablename + " where " + condition + " fetch first {} rows only".format(rows)


        try:
            #print(query)
            final =[]
            #print(query)
            cursor.arraysize = array_size
            results = cursor.execute(query)



            for i in results:
                final.append(i)

            connection.close()

            return  final

        except Exception as  e:

            #closing the connection and cursor

            connection.close()

            return {self.query_flag: str(sys.exc_info()[1])}

    def fetch_manyy1(self,columns,tablename,array_size,rows= -1,condition=None, conditionn = None):
        """
        Function to fetch all the values from a given table with a given condition

        :param columns : Tuple : Columns in the table you want to view
        :param tablename: String : Tablename in the DB
        :param condition: String : Where condition to filter in the Table
        :param array_size : Integer: Array Size of the cursor : Batch size
        :param rows : Integer: Number of rows to be fetched
        :return: A list of tuples  with values in order according to the columns selected from the given tablename, condition

        """


        #connecting to the DB

        try:

            connection = connect_oracle(self.username, self.pwd, self.host, self.port, self.sid)
            cursor = connection.cursor()

        except Exception as e:

            return {self.db_flag: str(sys.exc_info()[1])}



        #converting tuples to string
        cols = ""
        for i in columns:
            cols = cols + i + ","

        cols=cols[:len(cols) - 1]

        #2 different queries : with and without where clause

        if condition == None:

            if rows == -1:
                query = "Select " + cols + " from " + tablename
            else:
                query = "Select " + cols + " from " + tablename + " fetch first {} rows only".format(rows)


        else:
            if rows ==-1:
                #query = "Select " + cols + " from " + tablename + " where " + condition
                query = "SELECT \
                case when t.fixingtype  = a.ofixingtype then 'matching' else t.fixingtype end as fixingtypeStatus, \
                case when t.fixingtype  = a.ofixingtype then 'matching' else a.ofixingtype end as fixingtypeStatus, \
                case when t.itemqty  = a.oitemqty then 'matching' else t.itemqty end as itemqtyStatus, \
                case when t.itemqty  = a.oitemqty then 'matching' else a.oitemqty end as itemqtyStatus, \
                case when t.itemprice  = a.itemvalue then 'matching' else t.itemprice end as itemValueStatus, \
                case when t.itemprice  = a.itemvalue then 'matching' else a.itemvalue end as itemValueStatus, \
                case when t.itemvol  = a.itemvolume then 'matching' else t.itemvol end as itemVolumeStatus, \
                case when t.itemvol  = a.itemvolume then 'matching' else a.itemvolume end as itemVolumeStatus, \
                case when t.customerarea  = a.cuarea then 'matching' else t.customerarea end as CustomerareaStatus, \
                case when t.customerarea  = a.cuarea then 'matching' else a.cuarea end as CustomerareaStatus, \
                case when t.customerareacode  = a.cuareacode then 'matching' else t.customerareacode end as CustomerareacodeStatus, \
                case when t.customerareacode  = a.cuareacode then 'matching' else a.cuareacode end as CustomerareacodeStatus, \
                case when t.customername  = a.custname then 'matching' else t.customername end as CustomerNameStatus, \
                case when t.customername  = a.custname then 'matching' else a.custname end as CustomerNameStatus, \
                case when t.customerzone  = a.custzone then 'matching' else t.customerzone end as CustomerZoneStatus, \
                case when t.customerzone  = a.custzone then 'matching' else a.custzone end as CustomerZoneStatus, \
                case when t.invoiceno  = a.invnumber then 'matching' else t.invoiceno end as InvoiceNumberStatus, \
                case when t.invoiceno  = a.invnumber then 'matching' else a.invnumber end as InvoiceNumberStatus, \
                case when t.scheduleid  = a.schedulenumber then 'matching' else t.scheduleid end as ScheduleNumberStatus, \
                case when t.scheduleid  = a.schedulenumber then 'matching' else a.schedulenumber end as ScheduleNumberStatus \
                FROM (select o.invoicenumber, o.trucknumber,o.jobid,oi.itemqty as oitemqty, oi.itemvolume,o.customerarea as cuarea, \
                o.customerareacode as cuareacode,o.invoicenumber as invnumber,o.schedulenumber,o.slot as slt, \
                oi.itemcode ,oi.itemvalue, oi.itemdescription, oi.fixingtype as ofixingtype, o.customername as custname, o.customerzone as custzone \
                FROM unassigned_invoice o\
                INNER JOIN unassigned_item  oi\
                ON o.id = oi.unassignedinvoiceid \
                and o.jobid = oi.jobid \
                where o.jobid = "+ condition +") a \
                join  transaction t \
                on t.jobid = "+conditionn+" and a.jobid ="+condition+"\
                and t.invoiceno = a.invoicenumber \
                and t.itemcode = a.itemcode \
                order by a.invoicenumber desc"
            else:
                query = "Select " + cols + " from " + tablename + " where " + condition + " fetch first {} rows only".format(rows)


        try:
            #print(query)
            final =[]
            #print(query)
            cursor.arraysize = array_size
            results = cursor.execute(query)



            for i in results:
                final.append(i)

            connection.close()

            return  final

        except Exception as  e:

            #closing the connection and cursor

            connection.close()

            return {self.query_flag: str(sys.exc_info()[1])}


    def insert_one(self,tablename,object):
        """


        :param tablename: String : Tablename of the Database
        :param object: Dictionary : Format : {Column:Value}
        :return: True

        """


        #connecting to the DB

        try:

            connection = connect_oracle(self.username, self.pwd, self.host, self.port, self.sid)
            cursor = connection.cursor()

        except Exception as e:

            return {self.db_flag: str(sys.exc_info()[1])}


        #preparing the columns and values

        cols = ""
        for i in tuple(object.keys()):
            cols += str(i) + ","
        cols = "(" + cols[:len(cols) - 1] + ")"

        if len(object) == 1:
            if isinstance(list(object.values())[0], str):
                val = "('" + str(tuple(object.values())[0]) + "')"
            else:
                val = "(" + str(tuple(object.values())[0]) + ")"
        else:
            val = str(tuple(object.values()))
            print(val)

        query = "INSERT INTO " + tablename + cols + " values" + val + " returning id into :1"

        #print(query)

        try:

            new_id = cursor.var(cx_Oracle.NUMBER)
            cursor.execute(query, [new_id])

            connection.commit()
            connection.close()

            return int(new_id.getvalue()[0])

        except Exception as  e:

            #closing the connection and cursor

            connection.close()

            return {self.query_flag: str(sys.exc_info()[1])}



    def update_one(self,tablename,updations,condition=None):
          """

          :param tablename: String : Tablename of the DB
          :param updations: Object : Dictionary : Format : {column:value}
          :param condition: String : Where condition to filter in the Table
          :return: True

          """

          # connecting to the DB

          try:

              connection = connect_oracle(self.username, self.pwd, self.host, self.port, self.sid)
              cursor = connection.cursor()

          except Exception as e:

              return {self.db_flag: str(sys.exc_info()[1])}

            # preparing the columns and values

          cols = ""

          for i in updations.keys():
            substr = str(i) + " = " + str(updations[i]) + ","
            cols = cols + substr

          cols = cols[:len(cols) - 1]

         # query creation

          if condition == None:

              query = "Update " + tablename + " set " + cols

          else:
              query = "Update " + tablename + " set " + cols + " where " + condition

          try:

              cursor.execute(query)

              connection.commit()
              connection.close()

              return self.update_success_flag

          except Exception as  e:

              # closing the connection and cursor

              connection.close()

              return {self.query_flag: str(sys.exc_info()[1])}



    def insert_all(self,tablename,objects,condition=None,array_size=100000,bind_size=10000):
        """
          :param tablename: String : Tablename of the DB
          :param  Objects : List of Dictionary : Format : {column:value}
          :param condition: String : Where condition to filter in the Table
          :param array_size : Integer : The number of cells the cursor of the DB  should fetch in one call
          :param bind_size : Integer : The number of bindings of the object and the query should be done at a single call
          :return: True
        """

        # connecting to the DB

        try:

            connection = connect_oracle(self.username, self.pwd, self.host, self.port, self.sid)
            cursor = connection.cursor()

        except Exception as e:

            return {self.db_flag: str(sys.exc_info()[1])}

        # preparing the columns and placeholders

        val= objects[0]

        #columns

        cols = ""
        for i in tuple(val.keys()):
            cols += str(i) + ","
        cols = "(" + cols[:len(cols) - 1] + ")"

        #placeholders
        placeholders = ""
        for i in tuple(val.keys()):
            placeholders += ":" + str(i) + ","

        placeholders = "(" + placeholders[:len(placeholders) - 1] + ")"


        if condition == None:
            query = "INSERT INTO "+ tablename+ " " + cols + " values " + placeholders
        else:
            query = "INSERT INTO " + tablename + " " + cols + " values " + placeholders + " where " + condition

        try:

            cursor.bindarraysize = bind_size
            cursor.arraysize = array_size

            cursor.executemany(query,objects)


            connection.commit()
            connection.close()

            return self.insert_success_flag

        except Exception as  e:

            # closing the connection and cursor

            connection.close()

            return {self.query_flag: str(sys.exc_info()[1])}


    def update_all(self,query,updations,array_size=100000,bind_size=10000):
        """
          :param query : String : The SQL query with proper placeholders specified which are in the list of dictionaries
          :param updations : List of Dictionary : Format : {column:value}
          :param array_size : Integer : The number of cells the cursor of the DB  should fetch in one call
          :param bind_size : Integer : The number of bindings of the object and the query should be done at a single call
          :return: True
        """
        # connecting to the DB

        try:

            connection = connect_oracle(self.username, self.pwd, self.host, self.port, self.sid)
            cursor = connection.cursor()

        except Exception as e:

            return {self.db_flag: str(sys.exc_info()[1])}


        #executing the query

        try:

            cursor.bindarraysize = bind_size
            cursor.arraysize = array_size

            cursor.executemany(query,updations)


            connection.commit()
            connection.close()

            return self.update_success_flag

        except Exception as  e:

            # closing the connection and cursor

            connection.close()

            return {self.query_flag: str(sys.exc_info()[1])}


    def getJob(self):
        """
        Function to return the oldest job whose status is optimise
        """
        try:

            connection = connect_oracle(self.username, self.pwd, self.host, self.port, self.sid)
            cursor = connection.cursor()

        except Exception as e:

            return {self.db_flag: str(sys.exc_info()[1])}

        """
        Updating job and returning the id at the same time
        """
        try:

            job_id = cursor.callfunc("fetch_job", int)
            return job_id

        except Exception as e:
            return {self.db_flag: str(sys.exc_info()[1])}



class ApiSDK:

    def __init__(self):
        """
        initialising the global url for appending the necessary endpoints
        """
        # self.URL = os.environ.get('BASE_API_URL') + "/api/"

        # self.URL = "http://104.211.211.159:8000" + "/api/"

        # self.URL = "http://206.189.236.189:3000" + "/api/"

        # self.URL = "http://23.96.44.126:8000" + "/api/"

        self.URL = "http://159.89.164.133:3000" + "/api/"

    def postApi(self, endpoint, data):
        """
        :param endpoint: String: endpoint of the url
        :param data: list of dictionaries of the object
        :return: status code of the post request
        """
        # final url
        furl = self.URL + endpoint
       # print(furl)

        data = json.loads ( json.dumps ( data ) )

        response = rq.post(url = furl, json = data ) #data will be python dict


        return {"status":response.status_code,"responseData":response.json()}


#if __name__ == "__main__":

    #obj = SDK()
