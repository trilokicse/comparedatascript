import cx_Oracle
import sys

def connect_oracle(username,pwd,host,port,sid):
    dsn =   cx_Oracle.makedsn(host,port,service_name = sid).replace('SID','SERVICE_NAME')
    flag = -1
    try :
        o_connect = cx_Oracle.connect(username,pwd,dsn=dsn)
    except:
        print(sys.exc_info()[1])
        flag = "Connection error"
        return flag

    return o_connect
