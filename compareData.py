from sdk import SDK
from prettytable import PrettyTable
from colorama import Fore, Style
# from texttable import Texttable

db = SDK()

id = str(input("Enter jod id :"))

masterjobid = db.fetch_one(columns="masterjobid",tablename="job",condition=id)
masterjobid = str(masterjobid[0])
print("master job id :", masterjobid)


# Step 1 for "transaction", "transactionc","invalid_invoice"
tables = ["transaction", "transactionc","invalid_invoice"]
columname = "invoiceno"
uniqueCount1 = []
for i in range(len(tables)):
    distinct = db.distinctCount(columname = columname, tablename = tables[i], condition= masterjobid)
    print("Number of unique invoice from "+ tables[i] + ":", distinct)
    uniqueCount1.append(distinct)

# Step 2 for vehicle , invalid_vehicle
tables = ["vehicle", "invalid_vehicle"]
columname = "truckregname"
vehicleCount1 =[]
for i in range(len(tables)):
    distinct = db.distinctCount(columname = columname, tablename = tables[i], condition= masterjobid)
    print("Number of unique vehicle from "+ tables[i] + ":", distinct)
    vehicleCount1.append(distinct)

# Step 3
print("Total transaction count = ", (uniqueCount1[1]+uniqueCount1[2]))

#Step 4
print("Total Vehicle Count = ",(vehicleCount1[0]+vehicleCount1[1]))

#Step 5
tables = ["optimise_invoice", "unassigned_invoice"]
columname = "invoicenumber"
uniqueCount2 = []
for i in range(len(tables)):
    distinct = db.distinctCount(columname = columname, tablename = tables[i], condition= id)
    print("Number of unique invoicenumber from "+ tables[i] + ":", distinct)
    uniqueCount2.append(distinct)

UnionCount = db.distinctCount1(columname = columname, tablename = tables[i], condition= id)
if (UnionCount == uniqueCount1[0]):
    print(Fore.GREEN +"Unique invoice from optimise and unassigned is equal to transaction :", UnionCount, Style.RESET_ALL)
else:
    print(Fore.RED +"Unique invoice from optimise and unassigned is equal to transaction :", UnionCount, Style.RESET_ALL)

#Step 6
tables = ["optimise_vehicle", "unassigned_truck"]
columname = "truckregname"
uniqueCount3 = []
for i in range(len(tables)):
    distinct = db.distinctCount(columname = columname, tablename = tables[i], condition= id)
    print("Number of unique vehicle from "+ tables[i] + ":", distinct)
    uniqueCount3.append(distinct)

if ((uniqueCount3[0]+uniqueCount3[1]) == (vehicleCount1[0]+vehicleCount1[1])):
    print(Fore.GREEN + "Unique truck from optimise and unassigned is equal to total truck ",Style.RESET_ALL)
else:
    print(Fore.RED + "Unique truck from optimise and unassigned is equal to total truck ", Style.RESET_ALL)

#Step 7
numberOfTruckKPI = db.count(tablename= "truck_kpi", condition=id)
print("Number of Truck_KPI Count = ",numberOfTruckKPI)
opVehicleCount = db.count(tablename="optimise_vehicle",condition=id)
#print("optimise_vehicle count = ",opVehicleCount)

if (numberOfTruckKPI == opVehicleCount):
    print("numberOfTruckKPI is equal to opVehicleCount:",numberOfTruckKPI)
else:
    print("numberOfTruckKPI is not equal to opVehicleCount")

# Step 8
doLocationCount = db.distinctCount(columname = "name",tablename= "do_location", condition=id)#count(tablename= "do_location", condition=id)
print("do_location Count = ",doLocationCount)
hubCount = db.distinctCount(columname = "warehousecode",tablename="hub",condition=masterjobid)
print("Hub count = ",hubCount)

if (doLocationCount < hubCount):
    print("do_location count is less than hub count")
else:
    print("do_location count is not less than hub count")

# Step 9
globalKPICount = db.count(tablename="global_kpi", condition=id)
print("Global_KPI Count = ", globalKPICount)

if(globalKPICount==1):
    print("Correct")
else:
    print("Incorrect")

# Step 10
print("All Vehicles from Optimise_Vehicle :",uniqueCount3[0] )
col = ("a","b")
count = 0
data = db.fetch_many( columns=col,tablename="vehicle", array_size=1000, condition=id,conditionn = masterjobid)
table = ["TruckName","VTruckName","OTruckName","VDriverCode","ODriverCode","VTeamCode","OTeamCode","VTruckAreaCode","OTruckAreaCode",\
         "VTruckStartLoc","OTruckStartLoc","VTruckEndLoc","OTruckEndLoc","VCarpenter1EmpNo","OCarpenter1EmpNo","VCarpenter2EmpNo","OCarpenter2EmpNo",\
         "VCarpenter3EmpNo","OCarpenter3EmpNo","VCarpenter4EmpNo","OCarpenter4EmpNo","VCarpenter5EmpNo","OCarpenter5EmpNo","VCarpenter6EmpNo","OCarpenter6EmpNo"]
print("")
print("=====Status between Vehicle and Optimise_Vehicle=====")
print("")

for k in range(1,len(table)):
    count = 0
    for j in range(len(data)):
        if (table[k] == 'TruckName'):
            continue
        elif (data[j][k] == "matching"):
            count += 1
        else:
            print("truckname:",data[j][0],data[j][k], end = ", ")
    print("\nNumber of Mismatch " + table[k] + "=",abs(len(data)-count))
data1 = db.fetch_many1( columns=col,tablename="vehicle", array_size=1000, condition=id,conditionn = masterjobid)
table = ["TruckName","VTruckName","UTruckName","VDriverCode","UDriverCode","VTeamCode","UTeamCode","VTruckAreaCode","UTruckAreaCode",\
         "VTruckStartLoc","UTruckStartLoc","VTruckEndLoc","UTruckEndLoc","VCarpenter1EmpNo","UCarpenter1EmpNo","VCarpenter2EmpNo","UCarpenter2EmpNo",\
         "VCarpenter3EmpNo","UCarpenter3EmpNo","VCarpenter4EmpNo","UCarpenter4EmpNo","VCarpenter5EmpNo","UCarpenter5EmpNo","VCarpenter6EmpNo","UCarpenter6EmpNo"]
print("")
print("=====Status between Vehicle and Unassigned_Vehicle=====")
print("")
for k in range(1,len(table)):
    count = 0
    for j in range(len(data1)):
        if (data1[j][k] == "matching"):
            count += 1
        else:
            print("truckname:",data1[j][0],data1[j][k], end = ", ")
    print("\nNumber of Mismatch " + table[k] + "=",abs(len(data1)-count))


data2 = db.fetch_manyy(columns=col,tablename="transaction", array_size=1000, condition=id,conditionn = masterjobid)
table1 = ["TFixingType","OFixingType","TItemQty","OItemQty","TItemValue","OItemValue","TItemVolume","OItemVolume",\
          "TCustomerArea","OCustomerArea","TCustomerAreaCode","OCustomerAreaCode","TCustomerName","OCustomerName",\
          "TCustomerZone","OCustomerZone","TInvoiceNumber","OInvoiceNumber","TScheduleNumber","OScheduleNumber"]

print("")
print("======Status between Transaction and Optimise_Invoice, Optimise_Item======")
print("")
for k in range(len(table1)):
    count = 0
    for j in range(len(data2)):
        if (data2[j][k] == "matching"):
            count += 1
        else:
            print(data2[j][k], end = ", ")
    print("\nNumber of Mismatch " + table1[k] + "=",abs(len(data2)-count))

data3 = db.fetch_manyy1(columns=col,tablename="transaction", array_size=1000, condition=id,conditionn = masterjobid)
table1 = ["TFixingType","UFixingType","TItemQty","UItemQty","TItemValue","UItemValue","TItemVolume","UItemVolume",\
          "TCustomerArea","UCustomerArea","TCustomerAreaCode","UCustomerAreaCode","TCustomerName","UCustomerName",\
          "TCustomerZone","UCustomerZone","TInvoiceNumber","UInvoiceNumber","TScheduleNumber","UScheduleNumber"]
print("")
print("======Status between Transaction and Unassigned_Invoice, Unassigned_Item======")
print("")
for k in range(len(table1)):
    count = 0
    for j in range(len(data3)):
        if (data3[j][k] == "matching"):
            count += 1
        else:
            print(data3[j][k], end = ", ")
    print("\nNumber of Mismatch " + table1[k] + "=",abs(len(data3)-count))
# print("TruckName,","DriverCode,","TeamCode,","TruckAreaCode,","TruckStartLoc,","TruckEndLoc,","Carpenter1EmpNo","Carpenter2EmpNo","Carpenter3EmpNo","Carpenter4EmpNo","Carpenter5EmpNo","Carpenter6EmpNo")
# for i in data:
#     print(i)
# print("data:", data)
# print("len",len(data))
#t = PrettyTable(["TruckName","DriverCode","TeamCode","TruckAreaCode","TruckStartLoc","TruckEndLoc","Carpenter1EmpNo","Carpenter2EmpNo","Carpenter3EmpNo","Carpenter4EmpNo","Carpenter5EmpNo","Carpenter6EmpNo"])
# print("===========TABLE Status between Vehicle and Optimise_Vehicle==========")
# t = PrettyTable(table)
# for i in data:
#     t.add_row(i)
# print(t)
#
# print("===========TABLE Status between Vehicle and Unassigned_Vehicle==========")
#
# t = PrettyTable(table)
# for i in data1:
#     t.add_row(i)
# print(t)
#
# print("===========TABLE Status between Transaction and Optimise_Invoice, Optimise_Item==========")
#
# t1 = PrettyTable(table1)
# for i in data2:
#     t1.add_row(i)
# print(t1)
#
# print("===========TABLE Status between Transaction and Unassigned_Invoice, Unassigned_Item==========")
#
# t1 = PrettyTable(table1)
# for i in data2:
#     t1.add_row(i)
# print(t1)